package com.company.model;
 public class BankHolder {

    private Bank[] banks;

    public BankHolder(Bank[] banks) {
        this.banks = banks;
    }

    public void calc(String bankName, String currency, int amount) {
        for (Bank currentBank : banks) {
            if (currentBank.isSameAs(bankName)) {
                currentBank.convert(currency, amount);
            }
        }
    }
}
